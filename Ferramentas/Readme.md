# Ferramentas

Ferramentas para o desenvolvedor Drupal.

PS: O [DrupalTools](https://drupaltools.github.io) - A list of popular open source and free tools that can help people accomplish Drupal related tasks. - tem tambem muitos links interessantes


## Módulos
Módulos mais voltados para front-end e design.

- [Drupal.tv](https://drupal.tv/)


## Distribuições
Listagem de distribuições Drupal para as mais diversas necessidades.

- [aGov](https://www.drupal.org/project/agov)
Modelo para sites governamentais, desenvolvida e mantida pela comunidade.

- [Opigno LMS](https://www.drupal.org/project/opigno_lms)
Distro para EAD e ensino a distância em geral.

- [Open Social](https://www.drupal.org/project/social)
Crie seua rede social com Drupal.

- [Drutopia](https://www.drutopia.org)
Plataformas para criar site direcionado para organizações sem fins lucrativos.

- [Droopler](https://www.drupal.org/project/droopler)
Criar site rápidos e modelos para landing pages por padrão.


## Docker

- [docker4drupal](https://github.com/wodby/docker4drupal)

- [Drupal.tv](https://drupal.tv/)


## Hospedagem
Hospedagens onde você pode testar o CMS.

- [Pantheon](https://pantheon.io/)
