# Aprendizado

Sites, canais, repositórios, etc, para o aprendizado e melhoria da carreira como desenvolvedor Drupal.


- [Open Drupal · An open curriculum of Drupal course materials](https://opendrupal.org/)
- [Asociación Española de Drupal (AED) - Vídeos](https://asociaciondrupal.es/videos) 
- [Drupal.tv](https://drupal.tv/)
- [Acquia - Training](https://training.acquia.com/)


## Cursos

- [Acquia - Playlists](https://www.youtube.com/user/AcquiaTV/playlists)
- [DR8]  [Drupal 8 Theme Development | Drupal Online Training Classes from DrupalTutor](https://www.drupaltutor.com/courses/drupal-8/theme-development)
- [DR8]  [Drupal 8 theming - Watch + Learn](http://watch-learn.com/series/drupal-8-theming)
- [DR7]  [Aprende Drupal 7 Creando un Sistema de Noticias | Udemy](https://www.udemy.com/aprende-drupal-7-creando-un-sistema-de-noticias/)


### Youtube

- [DR8][pt-br] [Yuri Seki - Drupal 8: Curso completo](https://www.youtube.com/playlist?list=PLGThgBXty5x56903f4LaK9Nb_WxtAWtsF)
- [DR8][pt-br] [JUST DIGITAL](https://www.youtube.com/playlist?list=PLlfDnV2eObctb16ICqeyWvaz3OQt8xLU9)
- [DR7][pt-br] [Íparos - Drupal](https://www.youtube.com/playlist?list=PLVBFeeG0QsqnvP1Jm4mymcn0WV0Wd9P4M)
- [DR8]  [Real life Drupal 8 theming project](https://www.youtube.com/watch?v=JDl-K_yqWto&list=PL2FjJlpyDp0Pfy-nf7OXGesiNNDMhgXPv)
- [DR8]  [Drupal 8 Layout and Theming Course](https://www.youtube.com/watch?v=_3Kc_11hfrM&list=PLpVC00PAQQxG0sW9YOueVgouRp4aj1bng)
