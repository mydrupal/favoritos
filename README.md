
# Favoritos

Um olhar bem pessoal sobre o CMS Drupal, a perpectiva de um front-end e designer de interfaces.

Links para o aprendizado e referencias salvos durante o contato com a ferramenta.

#### [Aprendizado](https://gitlab.com/mydrupal/favoritos/tree/master/Aprendizado)
Sites, canais, repositórios, etc, para o aprendizado e melhoria da carreira como desenvolvedor Drupal.

#### [Agências](https://gitlab.com/mydrupal/favoritos/tree/master/Agencias)
Links para agências nacionais e internacionais, links também para desenvolvedores engajados que inspiram e contribuem muito com a comunidade.

#### [Ferramentas](https://gitlab.com/mydrupal/favoritos/tree/master/Ferramentas)
Ferramentas para o desenvolvedor Drupal.
