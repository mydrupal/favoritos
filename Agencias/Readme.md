# Agencias
Links para agências nacionais e internacionais, links também para desenvolvedores engajados que inspiram e contribuem muito com a comunidade.


## Cooperativas
Essas iniciativas são as mais interessantes ao meu ver pelo o modelo como se propõe contruir e difundir essa cultura.

- https://agile.coop
- https://agaric.coop


### Brasileiras
Ainda não catalogadas


### Geral

- https://www.mediacurrent.com
- https://evolvingweb.ca
- https://www.phase2technology.com
- https://www.lullabot.com
- https://ffwagency.com
- https://www.zivtech.com
- https://www.previousnext.com.au
- https://www.annertech.com
- https://www.ctidigital.com
- https://www.duo.be